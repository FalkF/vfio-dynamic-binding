#!/bin/bash

export VGA_DEVICE=0000:01:00.0
export AUDIO_DEVICE=0000:01:00.1
export VGA_DEVICE_ID=1002:67b1
export AUDIO_DEVICE_ID=1002:aac8
export SOUNDCARD=0000:05:01.0
export SOUNDCARD_ID=1102:0007


vfiounbind() {
	DEV="$1"

	echo -n Unbinding VFIO from ${DEV}...

	echo ${DEV} > /sys/bus/pci/drivers/vfio-pci/unbind
	sleep 1	

	echo > /sys/bus/pci/devices/${DEV}/driver_override
	echo 1 > /sys/bus/pci/devices/${DEV}/remove

	sleep 0.2

	echo OK!
}

pcirescan() {
	echo -n Rescanning PCI bus...

	su -c "echo 1 > /sys/bus/pci/rescan"
	sleep 0.2

	echo OK!

}

#i3-msg exit

# Xorg shouldn't run.
if [ -n "$( ps -C xinit | grep xinit )" ];
then
	echo "Don't run this inside Xorg!"
	exit 1
fi

lspci -nnkd $VGA_DEVICE_ID && lspci -nnkd $AUDIO_DEVICE_ID

echo Adios vfio, reloading the host drivers for the passedthrough devices...

sleep 0.5

vfiounbind $AUDIO_DEVICE
#echo "$VGA_DEVICE_ID" "$VGA_DEVICE" > /sys/bus/pci/drivers/amdgpu/new_id
vfiounbind $VGA_DEVICE
vfiounbind $SOUNDCARD

pcirescan

lspci -nnkd $VGA_DEVICE_ID && lspci -nnkd $AUDIO_DEVICE_ID

mv /etc/X11/xorg.conf.d/10-intel.conf /etc/X11/xorg.conf.d/10-intel.conf.b
mv /etc/X11/xorg.conf.d/20-radeon.conf.b /etc/X11/xorg.conf.d/20-radeon.conf

#su -l groot -c "startx"
#su -l groot -c "xrandr --output VGA1  --auto --primary"
#su -l groot -c "xrandr --output DVI-1-0 --auto --same-as VGA1"
#su -l groot -c "xrandr --output DisplayPort-1-0 --auto --left-of VGA1"
#su -l groot -c "i3-msg restart"
