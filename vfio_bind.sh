#!/bin/bash

export VGA_DEVICE=0000:01:00.0
export VGA_DEVICE_ID=1002:67b1
export AUDIO_DEVICE=0000:01:00.1
export AUDIO_DEVICE_ID=1002:aac8
#export USB_PCI_0=0000:05:02.0
#export USB_PCI_0_ID=1106:3038
#export USB_PCI_1=0000:05:02.1
#export USB_PCI_1_ID=1106:3038
#export USB_PCI_2=0000:05:02.2
#export USB_PCI_2_ID=1106:3104
#export USB_PCI_3=0000:05:02.3
#export USB_PCI_3_ID=1106:3044
export SOUNDCARD=0000:05:01.0
export SOUNDCARD_ID=1102:0007

vfiobind() {
	DEV="$1"

	# check if VFIO is already bound
	VFIODRV="$( ls -l /sys/bus/pci/devices/${DEV}/driver | grep vfio )"
	if [ -n "$VFIODRV" ];
	then
		echo VFIO was already bound to this device!
		return 0
	fi

	echo -n Binding VFIO to ${DEV}...

	echo ${DEV} > /sys/bus/pci/devices/${DEV}/driver/unbind
	sleep 1

	echo vfio-pci > /sys/bus/pci/devices/${DEV}/driver_override
	echo ${DEV} > /sys/bus/pci/drivers/vfio-pci/bind

	sleep 0.5

	echo OK!
}


#xrandr --output DisplayPort-1-0 --off
#xrandr --output DVI-1-0 --off
#i3-msg exit

# Xorg shouldn't run.
if [ -n "$( ps -C xinit | grep xinit )" ];
then
		echo "Don't run this inside Xorg!"
		exit 1
fi

lspci -nnkd $VGA_DEVICE_ID && lspci -nnkd $AUDIO_DEVICE_ID && lspci -nnkd $SOUNDCARD_ID

echo Binding devices to vfio

vfiobind $VGA_DEVICE
vfiobind $AUDIO_DEVICE
#vfiobind $USB_PCI_0
#vfiobind $USB_PCI_1
#vfiobind $USB_PCI_2
#vfiobind $USB_PCI_3
vfiobind $SOUNDCARD

lspci -nnkd $VGA_DEVICE_ID && lspci -nnkd $AUDIO_DEVICE_ID && lspci -nnkd $SOUNDCARD_ID

# use igpu
mv /etc/X11/xorg.conf.d/10-intel.conf.b /etc/X11/xorg.conf.d/10-intel.conf
mv /etc/X11/xorg.conf.d/20-radeon.conf /etc/X11/xorg.conf.d/20-radeon.conf.b
